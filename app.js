const express = require('express');
const cors = require('cors');
const cheerio = require('cheerio');
const axios = require('axios');
require('dotenv').config();
const redis = require('redis');

const app = express();

const port = process.env.PORT || 3030;
// const redisClient = redis.createClient({
// 	url: `redis://default:${process.env.REDIS_PASSWORD}@${process.env.REDIS_HOST}:${process.env.REDIS_PORT}`,
// });

const redisClient = redis.createClient({
	socket: {
		host: process.env.REDIS_HOST,
		port: process.env.REDIS_PORT,
	},
	password: process.env.REDIS_PASSWORD,
	legacyMode: true,
});

(async () => {
	await redisClient.connect();
})();

console.log('Connecting to the Redis');

redisClient.on('ready', () => {
	console.log('Connected!');
});

redisClient.on('error', (err) => {
	console.log('Error in the Connection');
});

app.use(cors());

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.get('/', async (req, res) => {
	let baseUrl = req.query.website;

	if (!baseUrl) {
		return res.status(200).send('welcome to css scrapper');
	}

	if (baseUrl.indexOf('?') >= 0) {
		baseUrl = baseUrl.substr(0, baseUrl.indexOf('?'));
	}

	if (baseUrl.indexOf('#') >= 0) {
		baseUrl = baseUrl.substr(0, baseUrl.indexOf('#'));
	}

	// redisClient.del(baseUrl);

	redisClient.get(baseUrl, async (error, cssStyles) => {
		if (error) {
			console.log('error: ', error);
		}
		if (cssStyles) {
			console.log('getting data from redis.... ');
			return res.json({
				success: true,
				website: baseUrl,
				data: `${cssStyles}`,
			});
		} else {
			console.log('getting data from server... ');
			const response = await axios.get(baseUrl, {
				headers: { 'Accept-Encoding': 'gzip,deflate,compress' },
			});
			const websiteHtml = response.data;
			const $ = cheerio.load(websiteHtml);
			// gets all the links with href in the head tag

			// set the data in the redis database
			redisClient.set(baseUrl, `${$('head').html()}`);

			res.json({
				success: true,
				website: baseUrl,
				data: `${$('head').html()}`,
			});
		}
	});
});

app.listen(port, () => {
	console.log(`server listening on http://localhost:${port}`);
});
